package GetData;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;

public class matchesWonPerTeamPerYear {
    public static String matchesWonPerTeamPerYear2(String matchesPath) {
        String RawData = "";
        int count = 0;
        int counter = 0;

        HashMap<String, Integer> result = new HashMap<String, Integer>();
        try {
//
            BufferedReader BufferedData = new BufferedReader(new FileReader(matchesPath));
            BufferedData.readLine();


            while ((RawData = BufferedData.readLine()) != null) {
//                System.out.println(line);
                String[] matchesData = RawData.split(",");
                String Winners = matchesData[10];
//                System.out.println(Winners);
                if (Winners.isEmpty()) {
                    counter++;
                } else if (result.containsKey(Winners)) {
                    result.put(Winners, result.get(Winners) + 1);
                } else {
                    result.put(Winners, 0);

                }

            }
//            System.out.println("No of matches empty " + counter);


        } catch (FileNotFoundException e) {
            return "FileNotFoundException";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result.toString();

    }



    public static void main(String[] args) {
        String matchesPath = "./data/matches.csv";
//        String deliveriesPath = "./data/deliveries.csv";
        matchesWonPerTeamPerYear obj1 = new matchesWonPerTeamPerYear();
        System.out.println(obj1.matchesWonPerTeamPerYear2(matchesPath));
    }
}




