package GetData;

import GetData.ExtraRunsConceded;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExtraRunsConcededTest {
    String matchesPath = "./data/matches.csv";
    String deliveriesPath = "./data/deliveries.csv";
    String expectedOutput = "{Gujarat Lions=98, Mumbai Indians=102, Sunrisers Hyderabad=107, Kings XI Punjab=100, Delhi Daredevils=106, Rising Pune Supergiants=108, Kolkata Knight Riders=122, Royal Challengers Bangalore=156}";

    @org.junit.Test
   public void case1() {
        ExtraRunsConceded obj1 = new ExtraRunsConceded();
        String actualOutput = obj1.ExtraRunsConceded3(matchesPath,deliveriesPath);
        assertEquals(expectedOutput, actualOutput);
    }
    @org.junit.Test
    public void case2(){
        ExtraRunsConceded obj1 = new ExtraRunsConceded();
        String expectedOutput = "FileNotFoundException";
        String actualOutput = obj1.ExtraRunsConceded3(" "," ");
        assertEquals(expectedOutput,actualOutput);
    }
}
