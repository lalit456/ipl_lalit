package GetData;

import java.io.*;
import java.util.*;

public class topEconomicalBowlers {
    public static String topEconomicalBowlers4(String matchesPath,String deliveriesPath) {

        String rawData = "";
        int count = 0;
        ArrayList<Float> sortedEconomyValues = new ArrayList<Float>();
        LinkedHashMap<String, Float> topEconomyBowlers = new LinkedHashMap<>();
        try {
            BufferedReader BufferedMatchesData = new BufferedReader(new FileReader(matchesPath));
            BufferedMatchesData.readLine();
            BufferedReader BufferedDeliveriesData = new BufferedReader(new FileReader(deliveriesPath));
            BufferedDeliveriesData.readLine();
            ArrayList<String> idYear = new ArrayList<String>();
            HashMap<String, Integer> runs = new HashMap<String, Integer>();
            HashMap<String, Integer> totalOvers = new HashMap<String, Integer>();
            HashMap<String, Float> economy = new HashMap<String, Float>();
            while ((rawData = BufferedMatchesData.readLine()) != null) {
                String[] matchesData = rawData.split(",");
                String seasons = matchesData[1];
                String matchId = matchesData[0];

                if (seasons.contains("2015")) {

                    idYear.add((matchId));
                }
            }
            while ((rawData = BufferedDeliveriesData.readLine()) != null) {
                String[] deliveriesData = rawData.split(",");

                String deliveriesId = deliveriesData[0];
                String bowlers = deliveriesData[8];
                String totalRuns = deliveriesData[17];

                if (idYear.contains((deliveriesId))) {
                    if (runs.containsKey(bowlers)) {
                        runs.put(bowlers, runs.get(bowlers) + Integer.parseInt(totalRuns));

                    } else {
                        runs.put(bowlers, Integer.parseInt(totalRuns));
                    }
                }
                if (idYear.contains(deliveriesId)) {
                    if (deliveriesData[5].contains("6")) {
                        if (totalOvers.containsKey(bowlers)) {
                            totalOvers.put(bowlers, totalOvers.get(bowlers) + 1);
                        } else {
                            totalOvers.put(bowlers, 1);
                        }
                    }

                }
            }
//            System.out.println(runs);
            for (String val : totalOvers.keySet()) {

                if (runs.containsKey(val)) {
//
                    economy.put(val, runs.get(val).floatValue() / totalOvers.get(val).floatValue());
//
//


                }
            }
            for (Float a : economy.values()) {
                sortedEconomyValues.add(a);
            }


            Collections.sort(sortedEconomyValues);
            for (Float e : sortedEconomyValues) {
                for (String bowlers : economy.keySet()) {
                    if (economy.get(bowlers) == e) {
//                        System.out.println(bowlers + " " + e);
                        topEconomyBowlers.put(bowlers, e);
                    }

                }
                if (topEconomyBowlers.size() == 10) {
                    break;
                }
            }



//            System.out.println(runs.size()+" "+overs.size());
        } catch (FileNotFoundException e) {
            return "FileNotFoundException";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return topEconomyBowlers.toString();
    }
    public static void main(String[] args) {
        String matchesPath = "./data/matches.csv";
        String deliveriesPath = "./data/deliveries.csv";
        topEconomicalBowlers obj1 = new topEconomicalBowlers();
        System.out.println(obj1.topEconomicalBowlers4(matchesPath,deliveriesPath));
    }

}
