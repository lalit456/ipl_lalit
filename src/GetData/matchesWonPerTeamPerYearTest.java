package GetData;

import GetData.matchesWonPerTeamPerYear;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class matchesWonPerTeamPerYearTest {
    String matchesPath = "./data/matches.csv";
    String expectedOutput = "{Mumbai Indians=91, Sunrisers Hyderabad=41, Pune Warriors=11, Rajasthan Royals=62, Kolkata Knight Riders=76, Royal Challengers Bangalore=72, Gujarat Lions=12, Rising Pune Supergiant=9, Kochi Tuskers Kerala=5, Kings XI Punjab=69, Deccan Chargers=28, Delhi Daredevils=61, Rising Pune Supergiants=4, Chennai Super Kings=78}";

    @org.junit.Test
    public void case1() {
        matchesWonPerTeamPerYear obj1 = new matchesWonPerTeamPerYear();
        String actualOutput = obj1.matchesWonPerTeamPerYear2(matchesPath);
        assertEquals(expectedOutput, actualOutput);
    }
    @Test
    public void case2(){
        matchesWonPerTeamPerYear obj1 = new matchesWonPerTeamPerYear();
        String expectedOutput = "FileNotFoundException";
        String actualOutput = obj1.matchesWonPerTeamPerYear2(" ");
        assertEquals(expectedOutput,actualOutput);
    }
}
