package GetData;
import java.io.*;
import java.util.*;

public class Temp {
    public static void main(String[] args) {
        String path = "./data/matches.csv";
        String path2 = "./data/deliveries.csv";
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            br.readLine();
            BufferedReader br1 = new BufferedReader(new FileReader(path2));
            br1.readLine();
            ArrayList<String> id = new ArrayList<String>();
            ArrayList<String> teams = new ArrayList<String>();
            HashMap<String, Integer> runs = new HashMap<String, Integer>();
            HashMap<String, Integer> overs = new HashMap<String, Integer>();
            HashMap<String, Float> economy = new HashMap<String, Float>();
            ArrayList<Float> sortedEconomyValues = new ArrayList<Float>();
            LinkedHashMap<String, Float> topEconomyBowlers = new LinkedHashMap<>();
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
//                System.out.println(Arrays.toString(data));
                if (data[1].contains("2015")) {
//                    System.out.println(i);
                    id.add((data[0]));
                }
            }
            while ((line = br1.readLine()) != null) {
                String[] data2 = line.split(",");
//                System.out.println(Arrays.toString(data2));
                if (id.contains((data2[0]))) {
                    if (runs.containsKey(data2[8])) {
                        runs.put(data2[8], runs.get(data2[8]) + Integer.parseInt(data2[17]));
                    } else {
                        runs.put(data2[8], Integer.parseInt(data2[17]));
                    }
                }
                if (id.contains(data2[0])) {
                    if (data2[5].contains("6")) {
                        if (overs.containsKey(data2[8])) {
                            overs.put(data2[8], overs.get(data2[8]) + 1);
                        } else {
                            overs.put(data2[8], 1);
                        }
                    }

                }
            }
            for (String val : overs.keySet()) {
//    System.out.println(runs.get(val)/overs.get(val));
                if (runs.containsKey(val)) {
//                    System.out.println(runs.get(val) + " " + overs.get(val));
                    economy.put(val, runs.get(val).floatValue() / overs.get(val).floatValue());
//                    System.out.println(economy);
//


                }
            }
            for (Float a : economy.values()) {
                sortedEconomyValues.add(a);
            }


            Collections.sort(sortedEconomyValues);
            for (Float e : sortedEconomyValues) {
                for (String bowlers : economy.keySet()) {
                    if (economy.get(bowlers) == e) {
//                        System.out.println(bowlers + " " + e);
                        topEconomyBowlers.put(bowlers, e);
                    }

                }
                if (topEconomyBowlers.size() == 10) {
                    break;
                }
            }

            System.out.println(topEconomyBowlers);

//            System.out.println(runs.size()+" "+overs.size());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
