package GetData;

import GetData.topEconomicalBowlers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class topEconomicalBowlersTest {
    String matchesPath = "./data/matches.csv";
    String deliveriesPath = "./data/deliveries.csv";
    String expectedOutput = "{RN ten Doeschate=4.0, J Yadav=4.142857, V Kohli=5.0, R Ashwin=5.8717947, S Nadeem=6.142857, Parvez Rasool=6.2, MC Henriques=6.56, Z Khan=6.625, M Vijay=7.0, GB Hogg=7.047619}";

    @Test
    public void case1() {
        topEconomicalBowlers obj1 = new topEconomicalBowlers();
        String actualOutput = obj1.topEconomicalBowlers4(matchesPath,deliveriesPath);
        assertEquals(expectedOutput, actualOutput);
    }
    @Test
    public void case2(){
        topEconomicalBowlers obj1 = new topEconomicalBowlers();
        String expectedOutput = "FileNotFoundException";
        String actualOutput = obj1.topEconomicalBowlers4(" "," ");
        assertEquals(expectedOutput,actualOutput);
    }
}
