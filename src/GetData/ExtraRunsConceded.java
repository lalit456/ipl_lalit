package GetData;
import java.io.*;
import java.util.*;

public class ExtraRunsConceded {
    public static String ExtraRunsConceded3(String matchesPath, String deliveriesPath){
        String rawData = "";
        int count = 0;
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        try {
            BufferedReader BufferedMatchesData = new BufferedReader(new FileReader(matchesPath));
            BufferedMatchesData.readLine();
            BufferedReader BufferedDeliveriesData = new BufferedReader(new FileReader(deliveriesPath));
            BufferedDeliveriesData.readLine();
            ArrayList<String> idYear = new ArrayList<String>();
//            ArrayList<String> matchesTeams = new ArrayList<String>();

            while ((rawData = BufferedMatchesData.readLine()) != null) {
                String[] matchesData = rawData.split(",");
                String seasons = matchesData[1];
                String matchId = matchesData[0];
                if (seasons.contains("2016")) {
                    idYear.add(matchId);
                }
//                System.out.println(matchId);
            }
            while ((rawData = BufferedDeliveriesData.readLine()) != null) {
                String[] deliveriesData = rawData.split(",");
                String deliveriesId = deliveriesData[0];
                String bowlingTeam = deliveriesData[3];
                if (idYear.contains(deliveriesId)) {
                    if (result.containsKey(bowlingTeam)) {
                        result.put(bowlingTeam, result.get(bowlingTeam) + Integer.parseInt(deliveriesData[16]));
                    } else {
                        result.put(bowlingTeam ,Integer.parseInt(deliveriesData[16]));
                    }
                }

            }

//


        } catch (FileNotFoundException e) {
            return "FileNotFoundException";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result.toString();
    }
    public static void main(String[] args) {
        String matchesPath = "./data/matches.csv";
        String deliveriesPath = "./data/deliveries.csv";
        ExtraRunsConceded obj1 = new ExtraRunsConceded();
        System.out.println(obj1.ExtraRunsConceded3(matchesPath, deliveriesPath));

    }
}
