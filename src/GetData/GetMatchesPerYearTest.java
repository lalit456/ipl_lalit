package GetData;
import GetData.GetMatchesPerYear;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GetMatchesPerYearTest {
    String matchesPath = "./data/matches.csv";
    String expectedOutput = "{2009=57, 2008=58, 2017=59, 2016=60, 2015=59, 2014=60, 2013=76, 2012=74, 2011=73, 2010=60}";
    @org.junit.Test
     public void case1(){
        GetMatchesPerYear obj1 = new GetMatchesPerYear();
        String actualOutput = obj1.getYearCount(matchesPath);
        assertEquals(expectedOutput,actualOutput);
    }
    @Test
    public void case2(){
        GetMatchesPerYear obj1 = new GetMatchesPerYear();
        String expectedOutput = "FileNotFoundException";
        String actualOutput = obj1.getYearCount(" ");
        assertEquals(expectedOutput,actualOutput);
    }


}