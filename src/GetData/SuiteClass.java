package GetData;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        matchesWonPerTeamPerYear.class,
        ExtraRunsConceded.class,
        topEconomicalBowlers.class,
        GetMatchesPerYear.class,
        matchesWonPerTeamPerYear.class
})

public class SuiteClass {

}
